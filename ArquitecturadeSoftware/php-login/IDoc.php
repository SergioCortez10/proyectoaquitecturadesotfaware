<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/estilo.css">
    <script src="ajax.js"></script>
    <title>Usfx</title>
</head>
<body>
   <section id="encabezado">
       <header>
           <button id="btn-nemu">
               <span id="menu-h"></span>
               <span id="menu-h"></span>
               <span id="menu-h"></span>
           </button>
           <a href="#">
               <img src="img/usfx_logo.jpg" alt="Logo de la universidad">
           </a>
       </header>
       <nav id="menu">
          <ul>
            <li><a id="materias" href="#" onclick="ajax(materias, 4)">materias</a></li>
            <li><a id="horarios" href="#" onclick="ajax(horarios, 4)">horarios</a></li>
            <li><a id="listas" href="#" onclick="ajax(listas, 4)">listas</a></li>
            <li><a id="actualizarnotas" href="#" onclick="ajax(actualizarnotas, 4) ">actualizar notas</a></li>
            <hr>
            <li><a id="perfil" href="#" onclick="ajax(perfil, 4)">perfil</a></li>
            <li><a id="notificaciones" href="#"onclick="ajax(notificaciones, 4)">notificaciones</a></li>
            <hr>
            <li><a href="logout.php">salir</a></li>
           </ul>
        </nav>
   </section>
    <section id="bienvenido">
      <div id="contenido">
        <h1>Gracias por estar en Janko_Estudios</h1>
        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Explicabo, cumque culpa. Excepturi provident odit quibusdam voluptas ipsum dolorem sit cum similique soluta! Iure commodi, magni expedita nostrum natus nobis sed.</p>
      </div>
   </section>  
</body>
</html>